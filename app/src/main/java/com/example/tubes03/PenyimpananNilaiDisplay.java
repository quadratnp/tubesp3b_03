package com.example.tubes03;

import android.content.Context;
import android.content.SharedPreferences;

public class PenyimpananNilaiDisplay {
    protected SharedPreferences sharedPref;
    protected final static String NAMA_SHARED_PREF = "SP_NILAI_DISPLAY";
    protected final static String KEY_KATEGORI = "KATEGORI";

    public PenyimpananNilaiDisplay(Context context){
        this.sharedPref = context.getSharedPreferences(NAMA_SHARED_PREF,0);
    }
    public void saveKategori(String nama){
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putString(KEY_KATEGORI, nama);
        editor.commit();
    }
    public void deleteKategori(){
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putString(KEY_KATEGORI, "");
        editor.commit();
    }

    public String getKategori(){
        return sharedPref.getString(KEY_KATEGORI,"");
    }
}
