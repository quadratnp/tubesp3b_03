package com.example.tubes03;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

public class FragmentLeft extends Fragment implements View.OnClickListener {
    protected Button btnHome;
    protected Button btnMap;
    protected Button btnList;
    protected Button btnNote;
    protected Button btnExit;
    public FragmentLeft(){}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_left, container, false);
        this.btnHome = view.findViewById(R.id.btn_Home);
        this.btnHome.setOnClickListener(this);

        this.btnMap = view.findViewById(R.id.btn_Map);
        this.btnMap.setOnClickListener(this);

        this.btnList = view.findViewById(R.id.btn_List);
        this.btnList.setOnClickListener(this);

        this.btnNote = view.findViewById(R.id.btn_note);
        this.btnNote.setOnClickListener(this);

        this.btnExit = view.findViewById(R.id.btn_exit);
        this.btnExit.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == this.btnHome.getId()){
            MainActivity mainActivity = (MainActivity)getActivity();
            mainActivity.changePage(1);
        }
        else if(v.getId() == this.btnMap.getId()){
            MainActivity mainActivity = (MainActivity)getActivity();
            mainActivity.changePage(2);
        }
        else if(v.getId() == this.btnList.getId()){
            MainActivity mainActivity = (MainActivity)getActivity();
            mainActivity.changePage(3);
        }
        else if(v.getId() == this.btnNote.getId()){
            MainActivity mainActivity = (MainActivity)getActivity();
            mainActivity.changePage(4);
        }
        else if(v.getId() == this.btnExit.getId()){
            MainActivity mainActivity = (MainActivity)getActivity();
            mainActivity.closeApplication();
        }
    }
}
