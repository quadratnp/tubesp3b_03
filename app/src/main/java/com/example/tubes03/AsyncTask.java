package com.example.tubes03;

import android.app.Activity;
import android.net.Uri;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class AsyncTask extends android.os.AsyncTask<Integer, Integer, String> {
    private FragmentMap mainActivity;
    private String BASE_URL = "https://bikewise.org/api/v2/locations/markers?";
    private String expression;
    private String incident_type= "incident_type";
    private String incident ;

    public AsyncTask(FragmentMap mainActivity, String incident){
        this.mainActivity = mainActivity;
        this.incident = incident;
    }

    @Override
    protected String doInBackground(Integer... integers) {
        String a = this.incident;
        Uri builtURI = Uri.parse(BASE_URL).buildUpon().appendQueryParameter(incident_type, a).build();
        Log.d("url", builtURI+ "");
        URL requestURL = null;
        try {
            requestURL = new URL(builtURI.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection conn = null ;
        try {
            conn =  (HttpURLConnection) requestURL.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        try {
            conn.setRequestMethod("GET");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
        conn.setDoInput(true);

        try {
            conn.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            int response = conn.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
        }

        InputStream is = null;
        try {
            is = conn.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String contentAsString = null;
        try {
            contentAsString = convertIsToString(is);
        } catch (IOException e) {
            e.printStackTrace();
        }

        finally {
            conn.disconnect();

            if(is!=null){
                try {
                    is.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
        Log.d("result", contentAsString);

        return contentAsString;




    }

    public String convertIsToString(InputStream stream)
            throws IOException, UnsupportedEncodingException {

        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

        String line;
        while((line = reader.readLine()) != null){
            stringBuilder.append(line);
        }

        if(stringBuilder.length() ==0){
            return null;
        }

        String res = stringBuilder.toString();
        return res;
    }


    @Override
    protected void onPostExecute(String s) {

        this.mainActivity.setResult(s);
    }
}
