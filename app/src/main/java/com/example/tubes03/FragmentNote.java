package com.example.tubes03;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class FragmentNote extends Fragment{
    protected TextView tvTitle;
    protected FragmentListener listener;
    public FragmentNote(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_note, container, false);
        this.tvTitle = view.findViewById(R.id.tvTitle);
        return view;
    }

    public static FragmentNote newInstance(String title){
        FragmentNote fragment = new FragmentNote();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof FragmentListener){
            this.listener = (FragmentListener) context;
        }
        else{
            throw new ClassCastException(context.toString()
                    + " must implement FragmentListener");
        }
    }
}
