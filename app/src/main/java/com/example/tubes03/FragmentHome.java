package com.example.tubes03;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;


import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class FragmentHome extends Fragment implements View.OnClickListener {
    protected TextView tvTitle;
    protected TextView tvTitle2;
    protected Button btn;
    protected FragmentListener listener;
    protected Spinner dropdown;
    protected PenyimpananNilaiDisplay nilai;
    public FragmentHome(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        this.tvTitle = view.findViewById(R.id.tvTitle);
        this.btn = view.findViewById(R.id.btnHome);
        this.dropdown = view.findViewById(R.id.spinner);
        this.nilai = new PenyimpananNilaiDisplay(this.getContext());
        getActivity().setTitle("Home");
        this.btn.setOnClickListener(this);

        return view;
    }

    public static FragmentHome newInstance(String title){
        FragmentHome fragment = new FragmentHome();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof FragmentListener){
            this.listener = (FragmentListener) context;
        }
        else{
            throw new ClassCastException(context.toString()
                    + " must implement FragmentListener");
        }
    }

    @Override
    public void onClick(View v) {
        if (this.nilai.getKategori() != "") {
            this.nilai.deleteKategori();
        }
        this.nilai.saveKategori(this.dropdown.getSelectedItem().toString());
        Log.d("kategori" , this.nilai.getKategori());
        this.listener.changePage(2);
    }


}
