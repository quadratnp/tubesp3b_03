package com.example.tubes03;

import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FragmentMap extends Fragment implements OnMapReadyCallback {
    protected TextView tvTitle;
    protected FragmentListener listener;
    GoogleMap map;
    Location[] lokasi;
    private FragmentActivity myContext;
    protected PenyimpananNilaiDisplay nilai;
    public FragmentMap(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        this.tvTitle = view.findViewById(R.id.tvTitle);
        this.lokasi = new Location[200];
        this.nilai = new PenyimpananNilaiDisplay(this.getContext());
        String kategori = this.nilai.getKategori();
        getActivity().setTitle("search results for " + kategori);
        Log.d("kategori", kategori);
        AsyncTask as = new AsyncTask(this, kategori);
        as.execute();

        return view;
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        for (int i = 0; i < this.lokasi.length; i++) {
            if (this.lokasi[i] != null) {
                Log.d("arr1", this.lokasi[i].getV() + ", " + this.lokasi[i].getV1());
                map.addMarker(new MarkerOptions().position(new LatLng(this.lokasi[i].getV(),
                        this.lokasi[i].getV1())).title("Rumah"));
                map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(this.lokasi[i].getV(),
                        this.lokasi[i].getV1())));
            }
        }


    }

    public static FragmentMap newInstance(String title){
        FragmentMap fragment = new FragmentMap();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        myContext=(FragmentActivity) context;
        super.onAttach(context);
        if(context instanceof FragmentListener){
            this.listener = (FragmentListener) context;
        }
        else{
            throw new ClassCastException(context.toString()
                    + " must implement FragmentListener");
        }
    }

    public void setResult(String res) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(res);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONArray hasil = null;
        try {
            hasil = (JSONArray) obj.get("features");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < hasil.length(); i++) {
            JSONObject jsonObject = hasil.optJSONObject(i);
            try {
                JSONObject jsonObject1 = jsonObject.getJSONObject("geometry");
                JSONArray jsonArray1 = jsonObject1.getJSONArray("coordinates");
                Log.d("coordinates", jsonObject1.length() + "");
                Double v = null;
                Double v1 = null;
                for (int j = 0; j < jsonArray1.length(); j++) {
                    if (j == 0) {
                        v = (Double) jsonArray1.get(j);
                        Log.d("v", v + "");
                    } else if (j == 1) {
                        v1 = (Double) jsonArray1.get(j);
                        Log.d("v1", v1 + "");
                    }
                }
                this.lokasi[i] = new Location(v1, v);

//                Log.d("obj" , jsonArray1 + "");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        for (int k = 0; k < this.lokasi.length; k++) {
            if (this.lokasi[k] != null) {
                Log.d("arr", this.lokasi[k].getV() + ", " + this.lokasi[k].getV1());
            }
        }
//        Iterator<Map.Entry> itr1 = hasil.entrySet().iterator();
//        while (itr1.hasNext()) {
//            Map.Entry pair = itr1.next();
//            if(pair.getKey().equals("coordinates")){
//                user_id_str = (String) pair.getValue();
//            }
//
//        }

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }
}