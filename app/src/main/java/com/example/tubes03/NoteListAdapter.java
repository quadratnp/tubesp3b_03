package com.example.tubes03;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;


public class NoteListAdapter extends BaseAdapter {
    protected  Activity activity;
    protected  List<Note> notes;
    protected  MainPresenter presenter;

    public NoteListAdapter(Activity activity, MainPresenter presenter){
        this.activity =  activity;
        this.presenter = presenter;
        this.notes = new LinkedList<Note>();
    }

    public void add(Note newItem){
        this.notes.add(newItem);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return this.notes.size();
    }

    @Override
    public Object getItem(int i) {
        return this.notes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.activity).inflate(R.layout.note_list_item, parent, false);
            viewHolder = new ViewHolder(convertView,this.presenter);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.updateView((Note)this.getItem(i),i);
        return convertView;
    }

    public void update(List<Note> getNotes){
        this.notes.clear();
        this.notes.addAll((getNotes));
        this.notifyDataSetChanged();
    }


    private class ViewHolder implements View.OnClickListener{
        protected TextView title, deskripsi;
        protected TextView detail;
        protected ImageButton btnDelete;
        protected MainPresenter presenter;
        protected ImageView ivFavourite;
        private int position;

        public ViewHolder(View view, MainPresenter presenter) {
            this.title = view.findViewById(R.id.tv_note_title);
            this.deskripsi = view.findViewById(R.id.tv_note_Deskripsi);
            this.btnDelete = view.findViewById(R.id.btn_delete);
            this.btnDelete.setOnClickListener(this);
            this.presenter = presenter;
        }

        public void updateView(Note note,int position){
            this.position = position;
            this.title.setText(note.getTitle());
            this.deskripsi.setText(note.getDeskripsi());
        }

        @Override
        public void onClick(View v) {

        }
    }
}