package com.example.tubes03;

public class Note {
    protected Integer id;
    protected String title;
    protected String deskripsi;

    public Note(Integer id, String title, String deskripsi) {
        this.id = id;
        this.title = title;
        this.deskripsi = deskripsi;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getDeskripsi() {
        return deskripsi;
    }
}
