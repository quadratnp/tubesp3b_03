package com.example.tubes03;

public class Location {
    protected Double v;
    protected Double v1;

    public Location (Double v , Double v1){
        this.v =v ;
        this.v1 = v1;
    }

    public Double getV() {
        return v;
    }

    public Double getV1() {
        return v1;
    }

    public void setV(Double v) {
        this.v = v;
    }

    public void setV1(Double v1) {
        this.v1 = v1;
    }
}
