package com.example.tubes03;

import android.database.Cursor;

public interface ISepeda {
    public Cursor read();
    public boolean create(Note note);
    public boolean update(Integer id, String title);
    public boolean delete(Integer id);
    public Cursor readData(String nama);
    public void loadData(Note note);
}
