package com.example.tubes03;

import java.util.List;

public interface IMainActivity {
    void updateList(List<Note> note);
}
