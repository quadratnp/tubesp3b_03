package com.example.tubes03;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;

import java.util.List;

public class MainActivity extends AppCompatActivity implements FragmentListener, IMainActivity {
    protected FragmentManager fragmentManager;
    protected FragmentHome fragmentHome;
    protected FragmentMap fragmentMap;
    protected FragmentList fragmentList;
    protected FragmentNote fragmentNote;
    protected FragmentAdd fragmentAdd;
    protected MainPresenter presenter;
    protected NoteListAdapter nla;
    protected DrawerLayout drawer;
    protected Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.presenter = new MainPresenter(getBaseContext(), this);
        this.nla = new NoteListAdapter(this, this.presenter);
        this.fragmentHome = FragmentHome.newInstance("New Fragment 1");
        this.fragmentMap = FragmentMap.newInstance("New Fragment 2");
        this.fragmentList = FragmentList.newInstance("New Fragment 3");
        this.fragmentNote = FragmentNote.newInstance("New Fragment 4");
        this.fragmentAdd = FragmentAdd.newInstance("new Fragment 5");
        this.fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction ft = this.fragmentManager.beginTransaction();
        ft.add(R.id.fragment_container,this.fragmentHome)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void changePage(int page){
        FragmentTransaction ft = this.fragmentManager.beginTransaction();

        if(page == 1){
            if(this.fragmentHome.isAdded()){
                ft.show(this.fragmentHome);
            }
            else {
                ft.add(R.id.fragment_container, this.fragmentHome)
                        .addToBackStack(null);
            }
            if(this.fragmentMap.isAdded()){
                ft.hide(this.fragmentMap);
            }
            if(this.fragmentList.isAdded()){
                ft.hide(this.fragmentList);
            }
            if(this.fragmentNote.isAdded()){
                ft.hide(this.fragmentNote);
            }
        }
        else if(page == 2){
            if(this.fragmentMap.isAdded()){
                ft.show(this.fragmentMap);
            }
            else{
                ft.add(R.id.fragment_container,this.fragmentMap)
                        .addToBackStack(null);
            }
            if(this.fragmentHome.isAdded()){
                ft.hide(this.fragmentHome);
            }
            if(this.fragmentList.isAdded()){
                ft.hide(this.fragmentList);
            }
            if(this.fragmentNote.isAdded()){
                ft.hide(this.fragmentNote);
            }
        }
        else if(page == 3){
            if(this.fragmentList.isAdded()){
                ft.show(this.fragmentList);
            }
            else{
                ft.add(R.id.fragment_container,this.fragmentList)
                        .addToBackStack(null);
            }
            if(this.fragmentHome.isAdded()){
                ft.hide(this.fragmentHome);
            }
            if(this.fragmentMap.isAdded()){
                ft.hide(this.fragmentMap);
            }
            if(this.fragmentNote.isAdded()){
                ft.hide(this.fragmentNote);
            }
        }

        else if(page == 4){
            if(this.fragmentNote.isAdded()){
                ft.show(this.fragmentNote);
            }
            else{
                ft.add(R.id.fragment_container,this.fragmentNote)
                        .addToBackStack(null);
            }
            if(this.fragmentHome.isAdded()){
                ft.hide(this.fragmentHome);
            }
            if(this.fragmentMap.isAdded()){
                ft.hide(this.fragmentMap);
            }
            if(this.fragmentList.isAdded()){
                ft.hide(this.fragmentList);
            }
            if(this.fragmentAdd.isAdded()){
                ft.hide(this.fragmentAdd);
            }
        }
        else if(page == 5){
            if(this.fragmentAdd.isAdded()){
                ft.show(this.fragmentAdd);
            }
            else{
                ft.add(R.id.fragment_container,this.fragmentAdd)
                        .addToBackStack(null);
            }
            if(this.fragmentHome.isAdded()){
                ft.hide(this.fragmentHome);
            }
            if(this.fragmentMap.isAdded()){
                ft.hide(this.fragmentMap);
            }
            if(this.fragmentList.isAdded()){
                ft.hide(this.fragmentList);
            }
            if(this.fragmentNote.isAdded()){
                ft.hide(this.fragmentNote);
            }
        }

        ft.commit();
    }

    @Override
    public void closeApplication() {
        this.moveTaskToBack(true);
        this.finish();
    }

    @Override
    public void updateList(List<Note> note) {
        this.nla.update(note);
    }
}