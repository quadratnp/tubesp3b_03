package com.example.tubes03;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

public class FragmentList extends Fragment implements IMainActivity, View.OnClickListener {
    protected ListView listNotes;
    protected TextView tvTitle;
    protected FragmentListener listener;
    protected ISepeda isepeda;
    protected NoteListAdapter nla;
    protected MainPresenter presenter;
    protected Button btn_add;
    protected Note note;
    protected IMainActivity im;

    public FragmentList(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        this.tvTitle = view.findViewById(R.id.tvTitle);
        this.listNotes = view.findViewById(R.id.lst_notes);
        this.presenter = new MainPresenter(this.getContext(), this.im);
        this.isepeda = new MainPresenter(this.getContext(), this.im);
        this.nla =new NoteListAdapter(this.getActivity(), this.presenter);
        this.btn_add = view.findViewById(R.id.btn_add);
        this.btn_add.setOnClickListener(this);
        read();
        this.listNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                note = (Note) listNotes.getItemAtPosition(position);
                Log.d("nama", "onItemClick: "+note.getTitle());
                Log.d("nama", "onItemClick: "+note.getId());
                Integer ide = note.getId();
                String[] pilihan = {"Ya","Tidak"};
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Pilihan");
                builder.setItems(pilihan, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                Log.d("id", "onClick: "+ide);
                                isepeda.delete(ide);
                                read();
                                break;
                            case 1:
                                dialogInterface.dismiss();
                                break;
                        }
                    }
                });
            builder.show();
            }
        });
        return view;
    }

    private void read(){
        //menuList = new ArrayList<>();

        this.isepeda = new MainPresenter(this.getContext(), this.im);
        Cursor cursor = this.isepeda.read();
        if(cursor.moveToFirst()){
            do{
                Note note = new Note(
                        Integer.valueOf(cursor.getString(0)),
                        cursor.getString(1),
                        cursor.getString(2)
                );
                this.nla.add(note);
                this.listNotes.setAdapter(this.nla);
            }
            while(cursor.moveToNext());
        }
        //lstMenus.setAdapter(mla);

    }

    public static FragmentList newInstance(String title){
        FragmentList fragment = new FragmentList();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof FragmentListener){
            this.listener = (FragmentListener) context;
        }
        else{
            throw new ClassCastException(context.toString()
                    + " must implement FragmentListener");
        }
    }

    @Override
    public void updateList(List<Note> note) {
        this.nla.update(note);
    }

    @Override
    public void onClick(View v) {
        MainActivity mainActivity = (MainActivity)getActivity();
        mainActivity.changePage(5);
    }
}
