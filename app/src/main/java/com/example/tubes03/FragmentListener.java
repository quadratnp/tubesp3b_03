package com.example.tubes03;

public interface FragmentListener {
    void changePage(int page);
    void closeApplication();}