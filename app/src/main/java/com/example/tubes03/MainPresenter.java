package com.example.tubes03;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MainPresenter extends SQLiteOpenHelper implements ISepeda {
    protected List<Note> notes;
    protected IMainActivity ui;

    public MainPresenter (Context context, IMainActivity aktifitas){
        super(context, "sepedaa.db", null,1);
        this.notes = new LinkedList<Note>();
        this.ui = aktifitas;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table menuu(id INTEGER PRIMARY KEY AUTOINCREMENT, title text, deskripsi text );";
        Log.d("Data", "onCreate: " + sql);
        db.execSQL(sql);
        sql = "INSERT INTO menuu (id, title, deskripsi) VALUES (null, 'judul A', 'deskripsi A');";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table menuu");
    }

    @Override
    public Cursor read() {
        SQLiteDatabase sql = getWritableDatabase();
        return sql.rawQuery("select * from menuu", null);
    }

    @Override
    public boolean create(Note note) {
        SQLiteDatabase sql = getWritableDatabase();
        sql.execSQL("insert into menuu values(null, '"+note.getTitle()+"', '"+note.getDeskripsi()+"')");
        return true;
    }

    @Override
    public boolean update(Integer id, String title) {
        SQLiteDatabase sql = getWritableDatabase();
        sql.execSQL("update menuu set title='"+title+"' where id = '"+id+"'");
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        SQLiteDatabase sql = getWritableDatabase();
        sql.execSQL("delete from menuu where id = '"+id+"'");
        return true;
    }

    public Cursor readData(String title){
        SQLiteDatabase sql = getWritableDatabase();
        return sql.rawQuery("select * from menuu where title ='"+title+"'", null);
    }

    public void loadData(Note note){
        this.notes.add(note);
        this.ui.updateList(this.notes);
    }
//    public MainPresenter(IMainActivity activity){
//        this.ui = activity;
//        this.notes = new LinkedList<Note>();
//    }
//    public void deleteList(int position){
//        this.notes.remove(position);
//        this.ui.updateList(this.notes);
//    }
//
//    public void addList(String title){
//        this.notes.add(new Note(title));
//        this.ui.updateList(this.notes);
//    }



}
