package com.example.tubes03;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import es.dmoral.toasty.Toasty;

public class FragmentAdd extends Fragment implements View.OnClickListener {

    protected FragmentListener listener;
    protected EditText et_title, et_deskripsi;
    protected Button btn_tambah;
    protected ISepeda isepeda;
    protected IMainActivity im;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add, container, false);
        this.et_title = view.findViewById(R.id.editText);
        this.et_deskripsi = view.findViewById(R.id.editText2);
        this.btn_tambah = view.findViewById(R.id.btn_tambahkan);
        this.btn_tambah.setOnClickListener(this);
        this.isepeda = new MainPresenter(this.getContext(), this.im );
        return view;
    }
    public static FragmentAdd newInstance(String title){
        FragmentAdd fragment = new FragmentAdd();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof FragmentListener){
            this.listener = (FragmentListener) context;
        }
        else{
            throw new ClassCastException(context.toString()
                    + " must implement FragmentListener");
        }
    }

    @Override
    public void onClick(View v) {
        create();
    }
    protected void create() {
        Note note = new Note(
                null,
                this.et_title.getText().toString(),
                this.et_deskripsi.getText().toString()
        );
        if (this.isepeda.create(note)) {
            Toasty.info(getContext(), "Berhasil Disimpan", Toast.LENGTH_SHORT).show();
            this.listener.changePage(4);
        }
    }
}
